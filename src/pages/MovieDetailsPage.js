/* eslint-disable react-hooks/exhaustive-deps */
import React, { useEffect } from 'react'
import { useParams } from 'react-router-dom'
import { Movie } from '../components';
import useFetch from '../hooks/useFetch';

export const MovieDetailsPage = () => {
  const params = useParams();
  const {data,isLoading,setUrl} = useFetch();
  useEffect(()=>{
    const movieId = params.id;
    const API_KEY = process.env.REACT_APP_API_KEY;
    const BASE_URL = process.env.REACT_APP_BASE_URL;
    const url = `${BASE_URL}/3/movie/${movieId}?api_key=${API_KEY}`;
    setUrl(url);
  },[]);
  return (
    <main>
      {isLoading && 'loading..'}
      {data && <Movie movie={data}/>}
    </main>
  )
}
