import React, { useEffect } from 'react'
import { MovieCard } from '../components/MovieCard'
import useFetch from '../hooks/useFetch';
import { Skeleton } from '../components';

export const MoviesPage = ({apipath}) => {

  const renderSkeleton = (nums=3) =>{
    const skeleton = [];
    for (let i = 0; i < nums; i++) {
     skeleton.push((<Skeleton />));
      
    }
    return skeleton;
  }
  

  const {data,isLoading,setUrl} = useFetch();

  useEffect(()=>{
    const API_KEY = process.env.REACT_APP_API_KEY;
    const BASE_URL = process.env.REACT_APP_BASE_URL;
    // console.log(BASE_URL);
    const url = `${BASE_URL}/3/movie${apipath}?api_key=${API_KEY}`;
    setUrl(url);
  // eslint-disable-next-line react-hooks/exhaustive-deps
  },[apipath]);
  return (
    <main>
      <div className="flex justify-start flex-wrap">
        {isLoading && renderSkeleton()}
       {data && data.results && data.results.map(movie =>(<MovieCard movie={movie}/>))}
      </div>
    </main>
  )
}
