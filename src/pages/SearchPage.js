import React, {  useEffect } from 'react'
import { useSearchParams } from 'react-router-dom'
import useFetch from '../hooks/useFetch';
import { MovieCard, Skeleton } from '../components';

export const SearchPage = () => {
    const [params] = useSearchParams();
    const queryString = params.get('q')
    const BASE_URL = process.env.REACT_APP_BASE_URL;
    const API_KEY = process.env.REACT_APP_API_KEY;

    const {data,isLoading,setUrl} = useFetch();

    useEffect(()=>{
        const URL = `${BASE_URL}/3/search/movie?api_key=${API_KEY}&query=${queryString}`;
setUrl(URL)
    // eslint-disable-next-line react-hooks/exhaustive-deps
    },[queryString])

    const renderSkeleton = (nums=3) =>{
        const skeleton = [];
        for (let i = 0; i < nums; i++) {
         skeleton.push((<Skeleton />));
        }
        return skeleton;
      }
  return (
    <main>
        <h1 className='text-3xl dark:text-white text-slate-800'>{
            data && data.results.length === 0 ? `NO movie found for ${queryString}`: `search results for: ${queryString}` 
        }</h1>
        <div className="flex justify-start flex-wrap">
            {isLoading && renderSkeleton()}
            {data && data.results && data.results.map(movie =>(<MovieCard movie={movie}/>))}
        </div>
    </main>
  )
}
