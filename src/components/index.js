export { Header } from './Header';
export { Footer } from './Footer';
export { MovieCard } from './MovieCard';
export { Skeleton} from './Skeleton';
export { Movie} from './Movie'