import React from 'react'
import { NavLink } from 'react-router-dom'

export const Footer = () => {
  return (
    <footer className="fixed bottom-0 left-0 z-20 w-full p-4 bg-white border-t border-gray-200 shadow dark:bg-slate-900 dark:border-slate-900 text-center">
    <span className="text-sm text-gray-500 sm:text-center dark:text-gray-400">©{(new Date()).getFullYear()} 
    <NavLink to='/' className="hover:underline">FilmFiesta&trade;</NavLink>. All Rights Reserved.
    </span>
  </footer>

  )
}
