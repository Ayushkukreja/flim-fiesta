import React from 'react'
import { Route, Routes } from 'react-router-dom'
import { MovieDetailsPage, MoviesPage, NotFoundPage, SearchPage } from '../pages'

export const AppRoutes = () => {
  return (
    <>
    <Routes>
        <Route path='/'  element={<MoviesPage apipath='/now_playing'/>} />
        <Route path='/movie/:id'  element={<MovieDetailsPage />} />
        <Route path='/movies/search'  element={<SearchPage />} />
        <Route path='/top-rated'  element={<MoviesPage apipath='/top_rated' />} />
        <Route path='/popular'  element={<MoviesPage apipath='/popular' />} />
        <Route path='/upcoming'  element={<MoviesPage apipath='/upcoming'/>} />
        <Route path='/*'  element={<NotFoundPage />} />
    </Routes>
    </>
  )
}
